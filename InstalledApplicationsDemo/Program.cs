﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstalledApplicationsDemo
{
    class Program
    {

        public static string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";

        static public bool IsInstalled(string program) 
        {
           
            using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
            {
                foreach (string subkey_name in key.GetSubKeyNames())
                {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                    {
                        if (subkey.GetValue("DisplayName") != null)
                        {
                            var type = subkey.GetValue("DisplayName").GetType();
                            if (subkey.GetValue("DisplayName").ToString() == program)
                            {
                                return true;
                            }
                        }

                    }
                }
                return false;
            }
        }

        static public string Menu() 
        {

           
            var program = Console.ReadLine();
            if (program != "exit") 
            {
                if (IsInstalled(program))
                    Console.WriteLine("Program: " + program + " Found!!");
                else
                    Console.WriteLine("Program: " + program + " Not Found!!");
            }
            
            return program;
        }

        static void Main(string[] args)
        {
            var input = "";
            Console.WriteLine("1. Type your wanted program's name");
            Console.WriteLine("2. Type exit to close the application");
            while (input != "exit") 
            {
                input = Menu();
            }
            
        }
    }
}
